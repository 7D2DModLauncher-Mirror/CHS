Welcome to the Classic Nomad Survival (CNS) mod, from sphereii and xyth, a new sphereii-mental Proof of Concept mod.

Enter our world, and test your survival skills in this limited-crafting Romero-style mod, inspired by Valmar's Classic Horde Survival mod.

******

Based on vanilla XML, with twists from the Magoli's Compo Pack and Tin's magical rwgmixer settings, CNS offers experienced players fresh new challenges, along with new mechanics that will influence the world and you. Challenge yourself and your friends as you fight for your very survival in this hardcore mod where loot and scoot is the name of the game. 

Code contributions from a variety of people, including StompyNZ, HAL9000, Carlzilla, stedman420, Mortelentus and TormentedEmu.

******

You've asked for this mod. And you probably asked the wrong people to make it. Start your regrets now through the 7D2D Mod Launcher, under the Sphereii-mental / CHS section.

Client Download:
-----------------

7D2D Mod Launcher:

Available through the Mod Launcher under the "sphereii-mental" section, called CNS.

Manual Download:
https://gitlab.com/7D2DModLauncher-Mirror/CHS/repository/master/archive.zip

Download and extract over top of a clean version of 7 Days to Die


Server Download:

Server Download is available in the above link. Copy over the Mods, Data, and 7DaysToDieServer_Data folder.
